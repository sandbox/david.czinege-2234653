(function($){
Drupal.behaviors.imagezoomer = {
  attach: function(context, settings) {
    var izf = settings.image_zoom_formatter;
    
    var params = {};
    
    // Tint
    if (izf.tint) {
      params.tint = true;
      params.tintColour = izf.tint_settings.tint_colour;
      params.tintOpacity = izf.tint_settings.tint_opacity;
    }
  
    $('.' + izf.attrClass).elevateZoom(params);
  }
};
})(jQuery);
