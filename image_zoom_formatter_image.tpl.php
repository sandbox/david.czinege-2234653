<?php

/**
 * Image Zoom Formatter Template File
 *
 * Variables:
 * $image_style_thumb
 * $image_style_zoomed
 * $item
 */

if ( ($image_style_thumb == '') || ($image_style_thumb == NULL) ){
  // Show the original image
  $image_style_thumb_url = file_create_url($item['uri']);
} else {
  // Thumbnail image style
  $image_style_thumb_url = image_style_url($image_style_thumb, $item['uri']);
}

if ( ($image_style_thumb == '') || ($image_style_thumb == NULL) ){
  // Show the original image  
  $image_style_zoomed_url = file_create_url($item['uri']);
} else {
  // Zoomed image style
  $image_style_zoomed_url = image_style_url($image_style_zoomed, $item['uri']);
}

$variables = array(
  'path'    => $image_style_thumb_url,
  'attributes' => array(
      'data-zoom-image' => $image_style_zoomed_url,
  ),
);

if ($item['alt'] != '') { $variables['alt'] = $item['alt']; }
if ($item['title'] != '') { $variables['title'] = $item['title']; }

print theme_image($variables);

?>