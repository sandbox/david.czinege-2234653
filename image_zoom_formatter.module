<?php

/**
 * Project: Image Zoom Formatter
 * Contributor: David Czinege <czinege.david.89@gmail.com>
 * Made in 2014
 */

/**
 * Implements hook_field_formatter_info().
 */
function image_zoom_formatter_field_formatter_info() {
  $info = array(
    'image_zoom_formatter' => array(
      'label' => t('Image Zoom Formatter'),
      'field types' => array('image'),
      'settings' => array(
        'image_style_thumb' => '',
        'image_style_zoomed' => '',
        'tint' => '',
        'tint_settings' => '',
      ),
      'description' => t('It uses the Jquery Image Zoom Plugin.'),
    ),
  );
  return $info;
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function image_zoom_formatter_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $image_styles = image_style_options(FALSE, PASS_THROUGH);
  
  $element['image_style_thumb'] = array(
    '#title' => t('Thumbnail image style'),
    '#type' => 'select',
    '#default_value' => $settings['image_style_thumb'],
    '#empty_option' => t('None (original image)'),
    '#options' => $image_styles,
  );

  $element['image_style_zoomed'] = array(
    '#title' => t('Zoomed image style'),
    '#type' => 'select',
    '#default_value' => $settings['image_style_zoomed'],
    '#empty_option' => t('None (original image)'),
    '#options' => $image_styles,
  );

  $element['tint'] = array(
    '#type' => 'checkbox',
    '#title' => t('Tint'),
    '#description' => t('enable a tint overlay'),
    '#default_value' => ( (isset($settings['tint'])) && ($settings['tint'] != '') )? $settings['tint']  : 0,
    '#ajax' => array(
      'callback' => 'image_zoom_formatter_tint_ajax_callback',
      'wrapper' => 'tint-container',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );
  
  // If the $form_state contains the tint checkbox's value
  if ( isset($form_state['values']['fields'][$instance['field_name']]['settings_edit_form']['settings']['tint']) ){
    $tint = $form_state['values']['fields'][$instance['field_name']]['settings_edit_form']['settings']['tint'];
  } else {
    $tint = $element['tint']['#default_value'];
  }
  
  // If tint is enabled
  if ($tint) {
    $element['tint_settings'] = array(
      '#type' => 'fieldset',
      '#access' => $tint,
      '#title' => t('Tint settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#prefix' => '<div id="tint-container">',
      '#suffix' => '</div>',
    );
  
  } else {
    $element['tint_settings'] = array(
      '#markup' => '<div id="tint-container"></div>'
    );
  }
  
  $element['tint_settings']['tint_colour'] = array(
    '#type' => 'textfield',
    '#title' => t('Tint colour'),
    '#default_value' => ( (isset($settings['tint_settings']['tint_colour'])) && ($settings['tint_settings']['tint_colour'] != '') )? $settings['tint_settings']['tint_colour']  : '#333',
    '#description' => t('colour of the tint, can be #hex, word (red, blue), or rgb(x, x, x)'),
  );  
  
  $element['tint_settings']['tint_opacity'] = array(
    '#type' => 'textfield',
    '#title' => t('Tint opacity'),
    '#default_value' => ( (isset($settings['tint_settings']['tint_opacity'])) && ($settings['tint_settings']['tint_opacity'] != '') )? $settings['tint_settings']['tint_opacity']  : '0.4',
    '#description' => t('opacity of the tint'),
  );
  
  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function image_zoom_formatter_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $summary = array();

  $image_styles = image_style_options(FALSE, PASS_THROUGH);
  // Unset possible 'No defined styles' option.
  unset($image_styles['']);
  // Styles could be lost because of enabled/disabled modules that defines
  // their styles in code.
  if (isset($settings['image_style_thumb']) && isset($image_styles[$settings['image_style_thumb']])) {
    $summary[] = t('Thumbnail image style: @style', array('@style' => $image_styles[$settings['image_style_thumb']]));
  }
  else {
    $summary[] = t('Thumbnail image style @style', array('@style' => 'original'));
  }

  if (isset($settings['image_style_zoomed']) && isset($image_styles[$settings['image_style_zoomed']])) {
    $summary[] = t('Zoomed image style: @style', array('@style' => $image_styles[$settings['image_style_zoomed']]));
  }
  else {
    $summary[] = t('Zoomed image style: @style', array('@style' => 'original'));
  }
  
  if ( (isset($settings['tint'])) && ($settings['tint'] == TRUE) ){
    $summary[] = t('Tint: enabled');
  } else {
    $summary[] = t('Tint: disabled');
  }
  
  return implode('<br />', $summary);
}

/**
* Implements hook_field_formatter_view().
*/
function image_zoom_formatter_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
 $element = array();
  switch ($display['type']) {
    case 'image_zoom_formatter':
    
      // Settings 
      $image_style_thumb = isset($display['settings']['image_style_thumb']) ? $display['settings']['image_style_thumb'] : NULL;
      $image_style_zoomed = isset($display['settings']['image_style_zoomed']) ? $display['settings']['image_style_zoomed'] : NULL;
      $tint = isset($display['settings']['tint']) ? $display['settings']['tint'] : 0;
      $tint_settings = ( (isset($display['settings']['tint'])) && ($display['settings']['tint'] == 1))  ? $display['settings']['tint_settings'] : 0;

      foreach ($items as $delta => $item) {
        $element[$delta] = array(
          '#theme' => 'image_zoom_formatter_image',
          '#image_style_thumb' => $image_style_thumb,
          '#image_style_zoomed' => $image_style_zoomed,
          '#item' => $item,
          '#class' => $instance['entity_type'] . '-' . $instance['bundle'] . '-' . $instance['field_name'],
          '#tint' => $tint,
          '#tint_settings' => $tint_settings,
        );
      }
      break;
  }
  
  return $element;
}

/**
* Implements hook_theme().
*/
function image_zoom_formatter_theme() {
  return array(
    'image_zoom_formatter_image' => array(
      'variables' => array(
        'image_style_thumb' => NULL,
        'image_style_zoomed' => NULL,
        'item' => NULL,
        'class' => NULL,
        'tint' => NULL,
        'tint_settings' => NULL,
      ),
    ),
  );
}

function theme_image_zoom_formatter_image($variables){
  drupal_add_library('image_zoom_formatter', 'elevatezoom');
  $image_zoom_formatter_options = array(
    'attrClass' => $variables['class'],
    'tint' => $variables['tint'],
    'tint_settings' => $variables['tint_settings'],
  );
  drupal_add_js(array('image_zoom_formatter' => $image_zoom_formatter_options), 'setting');
  drupal_add_js(drupal_get_path('module', 'image_zoom_formatter') . '/js/image_zoom_formatter.js');

  if ( ($variables['image_style_thumb'] == '') || ($variables['image_style_thumb'] == NULL) ){
    // Show the original image
    $image_style_thumb_url = file_create_url($variables['item']['uri']);
  } else {
    // Thumbnail image style
    $image_style_thumb_url = image_style_url($variables['image_style_thumb'], $variables['item']['uri']);
  }
  
  if ( ($variables['image_style_zoomed'] == '') || ($variables['image_style_zoomed'] == NULL) ){
    // Show the original image  
    $image_style_zoomed_url = file_create_url($variables['item']['uri']);
  } else {
    // Zoomed image style
    $image_style_zoomed_url = image_style_url($variables['image_style_zoomed'], $variables['item']['uri']);
  }
    
  $var = array(
    'path'    => $image_style_thumb_url,
    'attributes' => array(
        'data-zoom-image' => $image_style_zoomed_url,
        'class' => $variables['class'],
    ),
  );
    
  if ($variables['item']['alt'] != '') { $var['alt'] = $variables['item']['alt']; }
  if ($variables['item']['title'] != '') { $var['title'] = $variables['item']['title']; }
    
  return theme_image($var);
}

/**
 * Implements hook_libraries_info().
 */
function image_zoom_formatter_library() {
  $libraries['elevatezoom'] = array(
    'title' => 'jQuery Image Zoom Plugin', 
    'website' => 'http://www.elevateweb.co.uk/image-zoom/download', 
    'version' => '3.0.8',
    'js' => array(
      'sites/all/libraries/elevatezoom/jquery.elevatezoom.js' => array(),
    ),
  );

  return $libraries;
}

/**
* Callback element needs only select the portion of the form to be updated.
* Since #ajax['callback'] return can be HTML or a renderable array (or an
* array of commands), we can just return a piece of the form.
*/
function image_zoom_formatter_tint_ajax_callback($form, $form_state) {
  return $form['fields'][$form_state['triggering_element']['#parents'][1]]['format']['settings_edit_form']['settings']['tint_settings'];
}